# www.fetchPad 

Get pads contents with fetch function. 

Examples: 

css pad -> https://hebdo.framapad.org/p/fetchpad_css-9ya9  

html pad -> https://hebdo.framapad.org/p/fetchpad_html-9ya9  

